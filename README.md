# gitlab-cicd-deploy-docker-with-ansible-in-your-homelab

Deploy your docker-compose stacks with Gitlab CI/CD and Ansible.
Context can be found here: [https://boup.fr/posts/gitlab-cicd-deploy-docker-with-ansible-in-your-homelab/](https://boup.fr/posts/gitlab-cicd-deploy-docker-with-ansible-in-your-homelab/)

## Steps to use this project:
1. Create a new Gitlab project with these sources
1. Create an SSH key pair for Ansible
1. Encode the SSH private key file with `openssl base64 -in id_ed25519` and copy the base64 encode key to a `SSH_KEY` Gitlab CI/CD variable
1. Modify the `ansible/inventory` file to change the IP of the Docker stacks VM, you can also change the user that Ansible will use
1. Add the stacks names in `ansible/deploy.yaml` and `ansible/destroy.yaml` (see the "loop:" line)